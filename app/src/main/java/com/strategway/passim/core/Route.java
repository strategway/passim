/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim.core;

public class Route {

    // region Properties

    private String routeNumber;
    private String routeStartStation;
    private String routeTerminalStation;

    // endregion

    // region Constructor

    public Route(String pRouteNumber, String pRouteStartStation, String pRouteTerminalStation)
    {
        routeNumber = pRouteNumber;
        routeStartStation = pRouteStartStation;
        routeTerminalStation = pRouteTerminalStation;
    }

    // endregion

    // region Getters

    public String getRouteNumber()
    {
        return routeNumber;
    }

    public String getRouteStartStation()
    {
        return routeStartStation;
    }

    public String getRouteTerminalStation()
    {
        return routeTerminalStation;
    }

    // endregion
}
