/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim.core;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GPSTracker extends Service implements GpsStatus.Listener {

    // region properties

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;

    private AppOptions appOptions;

    private final Context mContext;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;

    // The minimum distance to change Updates in meters
    // TODO: Need to determine if we should set this at all.
    // When testing, it's helpful to see changes even if you are not moving in the room.
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 3;

    // Declaring a Location Manager
    protected LocationManager locationManager;

    private LocationListener locationListener;

    private List<ILocationObserver> observers;

    // Current coordinates
    private double latitude = -1;
    private double longitude = -1;

    // GPS status
    private GpsStatus mStatus;

    // endregion

    // region Constructor

    public GPSTracker(Context context) {
        this.mContext = context;
        appOptions = AppOptions.getInstance();
        observers = new ArrayList<ILocationObserver>();

        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                notifyObservers(location);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                switch(status) {
                    case LocationProvider.AVAILABLE:
                        for(int i = 0; i < observers.size(); i++) {
                            if(provider.equals(LocationManager.GPS_PROVIDER))
                                observers.get(i).onGPSStarted();
                            else if(provider.equals(LocationManager.NETWORK_PROVIDER)) {
                                observers.get(i).onNetworkStarted();
                            }
                        }
                        break;
                    default:
                        for(int i = 0; i < observers.size(); i++) {
                            if(provider.equals(LocationManager.GPS_PROVIDER))
                                observers.get(i).onGPSStopped();
                            else if(provider.equals(LocationManager.NETWORK_PROVIDER)) {
                                observers.get(i).onNetworkStopped();
                            }
                        }
                        break;
                }
            }

            @Override
            public void onProviderEnabled(String provider) {
                for(int i = 0; i < observers.size(); i++) {
                    if(provider.equals(LocationManager.GPS_PROVIDER))
                        observers.get(i).onGPSStarted();
                    else if(provider.equals(LocationManager.NETWORK_PROVIDER)) {
                        observers.get(i).onNetworkStarted();
                    }
                }
            }

            @Override
            public void onProviderDisabled(String provider) {
                for(int i = 0; i < observers.size(); i++) {
                    if(provider.equals(LocationManager.GPS_PROVIDER))
                        observers.get(i).onGPSStopped();
                    else if(provider.equals(LocationManager.NETWORK_PROVIDER)) {
                        observers.get(i).onNetworkStopped();
                    }
                }
            }
        };

        // check permission to access fine location
        if(ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(appOptions.getCurrentActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
                    );

        }
        else {

            // set location providers(both GPS and network)
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
        }
    }

    // endregion

    // region Implementation of ILocationObserver interface

    public void addObserver(ILocationObserver observer) {
        observers.add(observer);
    }

    public void removeObserver(ILocationObserver observer) {
        observers.remove(observer);
    }

    public void notifyObservers(Location location) {
        for(int i = 0; i < observers.size(); i++) {
            observers.get(i).onLocationChanged(location);
        }
    }

    // endregion

    // region Latitude and Longitude getters

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    // endregion

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public void onGpsStatusChanged(int event) {
        mStatus = locationManager.getGpsStatus(mStatus);
        /*switch (event) {
            case GpsStatus.GPS_EVENT_STARTED:
                for(int i = 0; i < observers.size(); i++) {
                    observers.get(i).onGPSStarted();
                }
                break;

            case GpsStatus.GPS_EVENT_STOPPED:
                for(int i = 0; i < observers.size(); i++) {
                    observers.get(i).onGPSStopped();
                }
                break;

            case GpsStatus.GPS_EVENT_FIRST_FIX:
                for(int i = 0; i < observers.size(); i++) {
                    observers.get(i).onGPSFixed();
                }
                break;

            case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                // Do Something with mStatus info
                break;
        }*/
    }

    public void requestLocationUpdates() {
        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, locationListener);
            Toast.makeText(appOptions.getCurrentActivity(), "Perm granted", Toast.LENGTH_SHORT).show();
        } catch (SecurityException e) {
            Toast.makeText(appOptions.getCurrentActivity(), "exception", Toast.LENGTH_SHORT).show();
        }
    }
}