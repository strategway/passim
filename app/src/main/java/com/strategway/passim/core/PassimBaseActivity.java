/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim.core;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.strategway.passim.FileSelectionActivity;
import com.strategway.passim.PrivacyPolicy;
import com.strategway.passim.R;
import com.strategway.passim.UserParamsActivity;
import com.strategway.passim.uitools.browser.BrowserHelper;

/**
 * Class that implements functions which are common for all activities of the application
 */
public abstract class PassimBaseActivity extends AppCompatActivity {

    // region properties

    protected AppOptions appOptions;
    protected Button backButton;
    protected Button forwardButton;

    // endregion

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());

        appOptions = AppOptions.getInstance();

        try {
            backButton = (Button) findViewById(R.id.buttonBack);
            forwardButton = (Button) findViewById(R.id.buttonForward);

            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    appOptions.backButtonPressed();
                }
            });

            forwardButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    appOptions.forwardButtonPressed();
                }
            });
        }
        catch(Exception e) {

        }

        appOptions.setCurrentActivity(this);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        appOptions.setCurrentActivity(this);
    }

    protected abstract int getLayoutResourceId();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_initial, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_file_selection) {
            Intent intent = new Intent(this, FileSelectionActivity.class);
            startActivity(intent);
            return true;
        }
        else if(id == R.id.action_user_params) {
            Intent intent = new Intent(this, UserParamsActivity.class);
            startActivity(intent);
            return true;
        }
        else if (id == R.id.action_about) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this)
                    .setTitle(R.string.about_title)
                    .setView(R.layout.dialog_about);
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
            alert.show();
            return true;
        }
        else if(id == R.id.action_privacy_policy) {
            BrowserHelper.openUriInBrowser(this, PrivacyPolicy.URL);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
