/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim.core;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.strategway.passim.DateTimeCheckActivity;
import com.strategway.passim.GPSCheckActivity;
import com.strategway.passim.PreparationActivity;
import com.strategway.passim.R;
import com.strategway.passim.RecommendationActivity;
import com.strategway.passim.WorkerActivity;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AppOptions extends Application {

    //region properties

    // Activities names
    private static final String InitialActivityName = "InitialActivity";
    private static final String DateTimeCheckActivityName = "DateTimeCheckActivity";
    private static final String GPSCheckActivityName = "GPSCheckActivity";
    private static final String PreparationActivityName = "PreparationActivity";
    private static final String RecommendationActivityName = "RecommendationActivity";
    private static final String WorkerActivityName = "WorkerActivityName";

    // Data
    private String workerName;
    private String workerEmail;
    private String workerLogin;
    private String workerPassword;
    private String bossEmail;
    private String serverAddress;
    private Route currentRoute;
    private String terminalStation;

    // Current activity
    private Activity currentActivity;

    private static Context mContext;
    public static AppOptions instance;

    private GPSTracker gps;

    // Instance of GPSTracker class
    public GPSTracker getGPSTracker() {
        if(gps == null) {
            gps = new GPSTracker(mContext);
        }
        return gps;
    };

    // Instance of class that handles HTTP requests to server
    public PassimServerClient client;

    // Result file
    private String resultFileName;

    // endregion

    // region App init methods

    public static Context getContext() {
        //  return instance.getApplicationContext();
        return mContext;
    }

    public static AppOptions getInstance()
    {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        String PREFS_NAME = "PassimPrefsFile";
        SharedPreferences settings;
        // initialize settings
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        String serverAddress;

        if(settings.contains(getString(R.string.server_name))) {

            serverAddress = settings.getString(getString(R.string.server_name), "");
        }
        else {
            SharedPreferences.Editor editor = settings.edit();
            serverAddress = getString(R.string.default_server_name);
            editor.putString(getString(R.string.server_name), serverAddress);
            editor.commit();
        }


        client = new PassimServerClient(serverAddress);
        instance = this;
        mContext = getApplicationContext();
    }

    // endregion

    // region App's activities management

    public Activity getCurrentActivity() {
        return  currentActivity;
    }

    public void setCurrentActivity(Activity pCurrentActivity) {
        currentActivity = pCurrentActivity;
    }

    public void backButtonPressed() {
        currentActivity.onBackPressed();
    }

    public void forwardButtonPressed() {
        switch (currentActivity.getClass().getSimpleName())
        {
            case PreparationActivityName:
                // save user's input in appOptions and start new activity
                if (((PreparationActivity) currentActivity).saveUserInputToOptions()) {
                    startActivityByClass(RecommendationActivity.class);
                }
                break;
            case RecommendationActivityName:
                startActivityByClass(DateTimeCheckActivity.class);
                break;
            case DateTimeCheckActivityName:
                startActivityByClass(GPSCheckActivity.class);
                break;
            case GPSCheckActivityName:
                if(!createNewFile()) {
                    Toast.makeText(this, getString(R.string.create_file_error_message), Toast.LENGTH_SHORT).show();
                    break;
                }
                startActivityByClass(WorkerActivity.class);
                break;
        }
    }

    private void startActivityByClass(Class activityClass) {
        Intent intent = new Intent(currentActivity.getBaseContext(), activityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(intent);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    // endregion

    // region Methods to handle Result file

    public boolean createNewFile() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateTime = sdf.format(new Date());
        resultFileName = getWorkerLogin() + "_" + currentDateTime + ".csv";

        try {
            FileOutputStream fos =  openFileOutput(resultFileName, MODE_PRIVATE);
            String headers = "Date" + "\t" +
                    "Time" + "\t" +
                    "Surveyor's login" + "\t" +
                    "Surveyor's name" + "\t" +
                    "Terminal station" + "\t" +
                    "Route's #" + "\t" +
                    "Passengers in" + "\t" +
                    "Passengers out" + "\t" +
                    "Latitude" + "\t" +
                    "Longitude";
            fos.write(headers.getBytes("UTF-16"));
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean saveChangesToFile(int currentPassengersInCount, int currentPassengersOutCount, double currentLatitude, double currentLongitude) {
        try {
            FileOutputStream fos =  openFileOutput(resultFileName, MODE_APPEND);

            String data = "\r\n";
            Date curDate = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String date = dateFormat.format(curDate);
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            String time = timeFormat.format(curDate);
            data += date + "\t" +
                    time + "\t" +
                    getWorkerLogin() + "\t" +
                    getWorkerName() + "\t" +
                    getTerminalStation() + "\t" +
                    String.valueOf(getCurrentRoute().getRouteNumber()) + "\t" +
                    String.valueOf(currentPassengersInCount) + "\t" +
                    String.valueOf(currentPassengersOutCount) + "\t" +
                    String.valueOf(currentLatitude) + "\t" +
                    String.valueOf(currentLongitude);

            fos.write(data.getBytes("UTF-16"));

            Toast.makeText(this, getString(R.string.file_save_success_message), Toast.LENGTH_SHORT).show();

            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean copyFileToUploadedFolder(String filename) {
        try {
            File uploadedFilesDir = getDir(getString(R.string.uploaded_files_dir), Context.MODE_PRIVATE);
            if(!uploadedFilesDir.exists()) {
                uploadedFilesDir.mkdir();
            }
            File source = new File(getFilesDir(), filename);
            File dest = new File(uploadedFilesDir, filename);
            FileUtils.copyFile(source, dest);

            source.delete();
            return true;
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Sends a letter with uploaded filename to foreman's email
     * @return
     */
    public boolean sendConfirmationToBossEmail(String filename, boolean setRoute) {
        try {

            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("plaint/text");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{getWorkerEmail(), getBossEmail()});
            if(setRoute) {
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, String.format(getString(R.string.email_subject),
                        getCurrentRoute().getRouteNumber(),
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(Calendar.getInstance().getTime())));
            }
            else {
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, String.format(getString(R.string.email_subject),
                        "<OLD_FILE>",
                        new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(Calendar.getInstance().getTime())));
            }
            emailIntent.putExtra(Intent.EXTRA_TEXT, String.format(getString(R.string.email_body), filename, client.BASE_URL));
            Intent intent = Intent.createChooser(emailIntent, getString(R.string.hint_send_file));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }
        catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, getString(R.string.file_sending_email_fail_message) +  e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    // endregion

    //region setters

    public void setWorkerName(String pWorkerName) {
        workerName = pWorkerName;
    }

    public void setWorkerEmail(String pWorkerEmail) {
        workerEmail = pWorkerEmail;
    }

    public void setWorkerLogin(String pWorkerLogin) { workerLogin = pWorkerLogin; }

    public void setWorkerPassword(String pWorkerPassword) { workerPassword = pWorkerPassword; }

    public void setBossEmail(String pBossEmail) {
        bossEmail = pBossEmail;
    }

    public void setServerAddress(String pServerName) {
        serverAddress = pServerName;
    }

    public void setCurrentRoute(Route pCurrentRoute) {
        currentRoute = pCurrentRoute;
    }

    public void setTerminalStation(String pTerminalStation) {
        terminalStation = pTerminalStation;
    }

    //endregion

    //region getters

    public String getWorkerName() {
        return workerName;
    }

    public String getWorkerEmail() {
        return workerEmail;
    }

    public String getWorkerLogin() { return workerLogin; }

    public String getWorkerPassword() { return workerPassword; }

    public String getBossEmail() {
        return bossEmail;
    }

    public String getServerAddress() {
        return serverAddress;
    }

    public Route getCurrentRoute() {
        return currentRoute;
    }

    public String getTerminalStation() {
        return terminalStation;
    }

    public String getResultFileName() { return resultFileName; }

    //endregion
}
