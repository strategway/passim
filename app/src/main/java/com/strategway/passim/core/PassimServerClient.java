/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim.core;

import android.content.Context;
import android.content.SharedPreferences;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.strategway.passim.R;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.auth.UsernamePasswordCredentials;
import cz.msebera.android.httpclient.impl.auth.BasicScheme;

public class PassimServerClient {

    // region properties

    public String BASE_URL = "http://passim.demo.strategway.com/";

    private AsyncHttpClient client;

    private boolean connected = false;

    // endregion

    // region Constructor

    public PassimServerClient(String url) {

        BASE_URL = url;

        // add "/" to the address if needed
        if (!BASE_URL.endsWith("/")) {
            BASE_URL += "/";
        }

        client = new AsyncHttpClient();
    }

    // endregion

    // region methods for HTTP requests

    public void connectToServer(String login, String password, AsyncHttpResponseHandler responseHandler) {


        client.get(BASE_URL, responseHandler);
    }

    public boolean isConnected() { return connected; }

    public void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(url, params, responseHandler);
    }

    private String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

    // endregion

    //region methods for file uploading

    public void uploadFile(String login, String password, File fileToUpload, AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        try {

            FileInputStream fin = new FileInputStream(fileToUpload);
            String ret = convertStreamToString(fin);

            //Make sure you close all streams.
            fin.close();
            params.put("file", fileToUpload);

            client.setBasicAuth(login, password);
            post(BASE_URL + "upload", params, responseHandler);

        } catch(FileNotFoundException e) {} catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    // endregion

    //region messages

    public String getFileSendingErrorMessage(Context context) {
        return context.getString(R.string.file_sending_server_fail_message);
    }

    public String getAuthErrorMessage(Context context) {
        return context.getResources().getString(R.string.auth_error_message);
    }

    public String getConnectionErrorMessage(Context context) {
        return context.getResources().getString(R.string.no_internet);
    }

    //endregion

}
