/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.strategway.passim.core.PassimBaseActivity;
import com.strategway.passim.core.PassimServerClient;
import com.strategway.passim.uitools.alert.AlertHelper;
import com.strategway.passim.uitools.browser.BrowserHelper;
import com.strategway.passim.uitools.linkliketext.LinkLikeText;

public class UserParamsActivity extends PassimBaseActivity {

    // region Properties

    private EditText workerNameEditText;
    private EditText workerEmailEditText;
    private EditText workerLoginEditText;
    private EditText workerPasswordEditText;
    private EditText bossEmailEditText;
    private EditText serverAddressEditText;
    private CheckBox privacyPolicyCheckbox;
    private TextView privacyPolicyLink;

    // shared preferences variables
    public static final String PREFS_NAME = "PassimPrefsFile";
    public SharedPreferences settings;

    // endregion

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize settings
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        workerNameEditText = (EditText)findViewById(R.id.workerNameEditText);
        workerEmailEditText = (EditText)findViewById(R.id.workerEmailEditText);
        workerLoginEditText = (EditText)findViewById(R.id.workerLoginEditText);
        workerPasswordEditText = (EditText)findViewById(R.id.workerPasswordEditText);
        bossEmailEditText = (EditText)findViewById(R.id.bossEmailEditText);
        serverAddressEditText = (EditText) findViewById(R.id.serverAddressEditText);
        privacyPolicyCheckbox = findViewById(R.id.privacyPolicyCheckbox);
        privacyPolicyLink = findViewById(R.id.privacyPolicyLink);

        privacyPolicyLink.setText(LinkLikeText.applyLinkStyle(this, R.string.privacy_policy_link));
        privacyPolicyLink.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                BrowserHelper.openUriInBrowser(UserParamsActivity.this, PrivacyPolicy.URL);
            }
        });

        // Getting data from shared preferences
        if(settings.contains(getResources().getString(R.string.worker_name_key)) &&
                settings.contains(getResources().getString(R.string.worker_email_key)) &&
                settings.contains(getResources().getString(R.string.boss_email_key)) &&
                settings.contains(getResources().getString(R.string.password)) &&
                settings.contains(getResources().getString(R.string.login)) &&
                settings.contains(getResources().getString(R.string.server_name))){
            String workerName  = settings.getString(getResources().getString(R.string.worker_name_key), "");
            String workerEmail = settings.getString(getResources().getString(R.string.worker_email_key), "");
            String bossEmail = settings.getString(getResources().getString(R.string.boss_email_key), "");
            String workerLogin = settings.getString(getResources().getString(R.string.login), "");
            String workerPassword = settings.getString(getResources().getString(R.string.password), "");
            String serverAddress = settings.getString(getResources().getString(R.string.server_name), "");

            workerNameEditText.setText(workerName);
            workerEmailEditText.setText(workerEmail);
            workerLoginEditText.setText(workerLogin);
            workerPasswordEditText.setText(workerPassword);
            bossEmailEditText.setText(bossEmail);

            InputFilter filter = new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end,
                                           Spanned dest, int dstart, int dend) {

                    if (source instanceof SpannableStringBuilder) {
                        SpannableStringBuilder sourceAsSpannableBuilder = (SpannableStringBuilder)source;
                        for (int i = end - 1; i >= start; i--) {
                            char currentChar = source.charAt(i);
                            if (!Character.isLetterOrDigit(currentChar)) {
                                sourceAsSpannableBuilder.delete(i, i+1);
                            }
                        }
                        return source;
                    } else {
                        StringBuilder filteredStringBuilder = new StringBuilder();
                        for (int i = start; i < end; i++) {
                            char currentChar = source.charAt(i);
                            if (Character.isLetterOrDigit(currentChar)) {
                                filteredStringBuilder.append(currentChar);
                            }
                        }
                        return filteredStringBuilder.toString();
                    }
                }
            };

            workerLoginEditText.setFilters(new InputFilter[] { filter });
            workerPasswordEditText.setFilters(new InputFilter[] { filter });
            serverAddressEditText.setText(serverAddress);

        } else {
            // set default server name in text field
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(getString(R.string.server_name), getString(R.string.default_server_name));
            serverAddressEditText.setText(getString(R.string.default_server_name));

            // set default login and password
            String defaultLogin = getResources().getString(R.string.default_login);
            String defaultPassword = getResources().getString(R.string.default_password);
            editor.putString(getResources().getString(R.string.login), defaultLogin);
            editor.putString(getResources().getString(R.string.password), defaultPassword);
            workerLoginEditText.setText(defaultLogin);
            workerPasswordEditText.setText(defaultPassword);

            editor.commit();
        }

    }

    /**
     * Email validation
     * @param target
     * @return
     */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    /**
     * Sets settings
     */
    private void setSettings() {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(getResources().getString(R.string.worker_name_key), appOptions.getWorkerName());
        editor.putString(getResources().getString(R.string.worker_email_key), appOptions.getWorkerEmail());
        editor.putString(getResources().getString(R.string.boss_email_key), appOptions.getBossEmail());
        editor.putString(getResources().getString(R.string.login), appOptions.getWorkerLogin());
        editor.putString(getResources().getString(R.string.password), appOptions.getWorkerPassword());
        editor.putString(getResources().getString(R.string.server_name), appOptions.getServerAddress());
        editor.commit();

        appOptions.client = new PassimServerClient(appOptions.getServerAddress());
    }

    public void onSaveParams(View v) {
        // get params from controls
        String workerName = workerNameEditText.getText().toString();
        String workerEmail = workerEmailEditText.getText().toString();
        String bossEmail = bossEmailEditText.getText().toString();
        String workerLogin = workerLoginEditText.getText().toString();
        String workerPassword = workerPasswordEditText.getText().toString();
        String serverAddress = serverAddressEditText.getText().toString();

        // check params
        if(workerName.length() == 0 || workerEmail.length() == 0 || bossEmail.length() == 0 ||
                workerLogin.length() == 0 || workerPassword.length() == 0 || serverAddress.length() == 0) {
            fillAllFieldsMessage();
            return;
        }

        if(!isValidEmail(workerEmail) || !isValidEmail(bossEmail)) {
            emailValidationFailedMessage();
            return;
        }

        if(!privacyPolicyCheckbox.isChecked()){
            AlertHelper.alert(this, R.string.privacy_policy_alert);
            return;
        }

        // save params and settings
        appOptions.setWorkerName(workerName);
        appOptions.setWorkerEmail(workerEmail);
        appOptions.setBossEmail(bossEmail);
        appOptions.setWorkerLogin(workerLogin);
        appOptions.setWorkerPassword(workerPassword);
        appOptions.setServerAddress(serverAddress);

        // set server name
        setSettings();

        Toast.makeText(this, getString(R.string.saving_user_params), Toast.LENGTH_SHORT).show();

        AlertHelper.yesNoAlert(this, R.string.geolocation_hint, R.string.geolocation_hint_accept, R.string.geolocation_hint_decline, new AlertHelper.YesNoDefaultCallback(){
            @Override public void yesClicked() {
                Intent intent = new Intent(UserParamsActivity.this, PreparationActivity.class);
                startActivity(intent);
            }
        });
    }


    public void onCancelClick(View v) {
        finish();
    }

    // region Messages

    private void fillAllFieldsMessage() {
        Toast.makeText(this, getResources().getString(R.string.fill_all_fields_message), Toast.LENGTH_SHORT).show();
    }

    private void emailValidationFailedMessage() {
        Toast.makeText(this, getResources().getString(R.string.email_validation_failed_message), Toast.LENGTH_SHORT).show();
    }

    // endregion

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_user_params;
    }
}
