/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.strategway.passim.core.PassimBaseActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateTimeCheckActivity extends PassimBaseActivity {

    // Current date and time broadcast receiver
    private final BroadcastReceiver TimeChangedReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(Intent.ACTION_TIME_CHANGED) || action.equals(Intent.ACTION_TIMEZONE_CHANGED) || action.equals(Intent.ACTION_TIME_TICK)) {

                Calendar calendar = Calendar.getInstance();
                ((TextView) findViewById(R.id.dateTextView)).setText(new SimpleDateFormat("dd.MM.yyyy").format(calendar.getTime()));

                ((TextView) findViewById(R.id.timeTextView)).setText(new SimpleDateFormat("HH:mm").format(calendar.getTime()));
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set date and time
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        registerReceiver(TimeChangedReceiver, intentFilter);

        Calendar calendar = Calendar.getInstance();

        ((TextView)findViewById(R.id.dateTextView)).setText(new SimpleDateFormat("dd.MM.yyyy").format(calendar.getTime()));

        ((TextView)findViewById(R.id.timeTextView)).setText(new SimpleDateFormat("HH:mm").format(calendar.getTime()));

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_date_time_check;
    }
}
