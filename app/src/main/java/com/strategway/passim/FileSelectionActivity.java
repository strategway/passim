/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.strategway.passim.core.PassimBaseActivity;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class FileSelectionActivity extends PassimBaseActivity {

    // region Properties

    private ListView generatedFilesListView;
    private ListView uploadedFilesListView;
    private TabHost mTabHost;

    private Button uploadFileButton;
    private Button deleteFileButton;

    private ArrayAdapter<String> generatedFilesAdapter;
    private ArrayAdapter<String> uploadedFilesAdapter;

    private List<String> generatedFilesNames;
    private List<String> uploadedFilesNames;

    private String selectedGeneratedFilename;
    private String selectedUploadedFilename;

    // progress dialogs
    private ProgressDialog authDialogProgress;
    private ProgressDialog uploadDialogProgress;

    private int currentTabIndex;

    // endregion

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uploadFileButton = (Button) findViewById(R.id.uploadFileButton);
        deleteFileButton = (Button) findViewById(R.id.deleteFileButton);
        uploadFileButton.setEnabled(false);
        deleteFileButton.setEnabled(false);

        selectedGeneratedFilename = "";
        selectedUploadedFilename = "";

        // Create tabs
        mTabHost = (TabHost) findViewById(R.id.tabHost);
        mTabHost.setup();

        TabHost.TabSpec spec = mTabHost.newTabSpec(getResources().getString(R.string.not_uploaded_files));
        spec.setContent(R.id.generatedFilesLinearLayout);
        spec.setIndicator(getResources().getString(R.string.not_uploaded_files));
        mTabHost.addTab(spec);

        spec = mTabHost.newTabSpec(getResources().getString(R.string.uploaded_files));
        spec.setContent(R.id.uploadedFilesLinearLayout);
        spec.setIndicator(getResources().getString(R.string.uploaded_files));
        mTabHost.addTab(spec);

        // Get filenames and fill the tabs
        getFilesNames();

        generatedFilesAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, generatedFilesNames);
        uploadedFilesAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, uploadedFilesNames);

        generatedFilesListView = (ListView) findViewById(R.id.generatedFilesListView);
        generatedFilesListView.setAdapter(generatedFilesAdapter);
        generatedFilesListView.setSelector(android.R.color.holo_blue_light);
        generatedFilesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedGeneratedFilename = (String) generatedFilesListView.getItemAtPosition(position);
                deleteFileButton.setEnabled(true);
                uploadFileButton.setEnabled(true);
            }
        });

        uploadedFilesListView = (ListView) findViewById(R.id.uploadedFilesListView);
        uploadedFilesListView.setAdapter(uploadedFilesAdapter);
        uploadedFilesListView.setSelector(android.R.color.holo_blue_light);
        uploadedFilesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectedUploadedFilename = (String) uploadedFilesListView.getItemAtPosition(position);
                deleteFileButton.setEnabled(true);
            }
        });

        // show filenames of current tab
        mTabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

                currentTabIndex = mTabHost.getCurrentTab();

                if(currentTabIndex == 0) {
                    uploadFileButton.setEnabled(true);
                    uploadFileButton.setVisibility(View.VISIBLE);
                }
                else if(currentTabIndex == 1) {
                    uploadFileButton.setEnabled(false);
                    uploadFileButton.setVisibility(View.INVISIBLE);
                }
            }
        });

        uploadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedGeneratedFilename.length() == 0) {
                    return;
                }

                uploadFile(selectedGeneratedFilename);
            }
        });

        deleteFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentTabIndex == 0) {
                    if(selectedGeneratedFilename.length() > 0) {
                        new AlertDialog.Builder(FileSelectionActivity.this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle(getString(R.string.file_delete))
                                .setMessage(getString(R.string.file_delete_confirmation))
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        File fileToDelete = new File(getFilesDir(), selectedGeneratedFilename);
                                        fileToDelete.delete();
                                        generatedFilesNames.remove(selectedGeneratedFilename);
                                        selectedGeneratedFilename = "";
                                        deleteFileButton.setEnabled(false);
                                        uploadFileButton.setEnabled(false);
                                        updateListViews();
                                    }
                                })
                                .setNegativeButton(getString(R.string.no), null)
                                .show();
                    }
                }
                else {
                    if(selectedUploadedFilename.length() > 0) {
                        new AlertDialog.Builder(FileSelectionActivity.this)
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setTitle(getString(R.string.file_delete))
                                .setMessage(getString(R.string.file_delete_confirmation))
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        File uploadedFilesDir = getDir(getString(R.string.uploaded_files_dir), Context.MODE_PRIVATE);
                                        File fileToDelete = new File(uploadedFilesDir, selectedUploadedFilename);
                                        fileToDelete.delete();
                                        uploadedFilesNames.remove(selectedUploadedFilename);
                                        selectedUploadedFilename = "";
                                        updateListViews();
                                        deleteFileButton.setEnabled(false);
                                    }

                                })
                                .setNegativeButton(getString(R.string.no), null)
                                .show();

                    }
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_file_selection;
    }

    /**
     * Gets filenames from both general folder and uploaded files folder in internal memory
     */
    private void getFilesNames() {

        generatedFilesNames = new ArrayList<String>();
        uploadedFilesNames = new ArrayList<String>();

        // get filenames that weren't sent on server
        File [] generatedFiles = getFilesDir().listFiles();

        for(int i = 0; i < generatedFiles.length; i++) {
            if(!generatedFiles[i].isDirectory()) {
                generatedFilesNames.add(generatedFiles[i].getName());
            }
        }

        // get filenames from uploaded files folder
        File uploadedFilesDir = getDir(getString(R.string.uploaded_files_dir), Context.MODE_PRIVATE);
        if(uploadedFilesDir.exists()) {
            File [] uploadedFiles = uploadedFilesDir.listFiles();
            for(int i = 0; i < uploadedFiles.length; i++) {
                uploadedFilesNames.add(uploadedFiles[i].getName());
            }
        }
    }

    private void updateListViews() {
        ((ArrayAdapter<String>) generatedFilesListView.getAdapter()).notifyDataSetChanged();
        ((ArrayAdapter<String>) uploadedFilesListView.getAdapter()).notifyDataSetChanged();
    }

    public void uploadFile(final String filename) {

        appOptions.client.uploadFile(appOptions.getWorkerLogin(), appOptions.getWorkerPassword(),
                new File(getBaseContext().getFilesDir() + File.separator + filename),
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                        // send a letter to foreman's email
                        appOptions.sendConfirmationToBossEmail(filename, false);

                        // copy file to another folder in internal memory
                        appOptions.copyFileToUploadedFolder(filename);

                        generatedFilesNames.remove(filename);
                        uploadedFilesNames.add(filename);
                        selectedGeneratedFilename = "";
                        updateListViews();
                        deleteFileButton.setEnabled(false);
                        uploadFileButton.setEnabled(false);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        String message;
                        if (responseBody == null) {
                            message = appOptions.client.getConnectionErrorMessage(FileSelectionActivity.this);
                        } else {
                            message = appOptions.client.getFileSendingErrorMessage(FileSelectionActivity.this);
                        }

                        Toast.makeText(FileSelectionActivity.this, message, Toast.LENGTH_SHORT).show();
                    }

                        @Override
                        public void onStart () {

                            uploadDialogProgress = ProgressDialog.show(FileSelectionActivity.this, getResources().getString(R.string.file_uploading_progress_message_title),
                                    getResources().getString(R.string.file_uploading_progress_message), true);
                        }

                    @Override
                    public void onFinish() {
                        uploadDialogProgress.dismiss();
                    }
                });
        }
    }
