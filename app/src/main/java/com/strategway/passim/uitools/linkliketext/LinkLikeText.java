package com.strategway.passim.uitools.linkliketext;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;

public class LinkLikeText {

    /**
     * @param context
     * @param insertToStringRes A string, applicable for String.format with exactly one %s qualifier. Note - %1$s and other details would not work
     * @param linkStringRes will be wrapped and putted instead of %s
     * @return
     */
    public static Spanned applyLinkStyleAndInsert(Context context, int insertToStringRes, int linkStringRes){
        return applyLinkStyleAndInsert(
                context,
                context.getResources().getString(insertToStringRes),
                context.getResources().getString(linkStringRes)
        );
    }

    /**
     * @param context
     * @param insertToString A string, applicable for String.format with exactly one %s qualifier. Note - %1$s and other details would not work
     * @param linkString will be wrapped and putted instead of %s
     * @return
     */
    public static Spanned applyLinkStyleAndInsert(Context context, String insertToString, String linkString){

        String placeholder = "%s";
        String[] parts = insertToString.split(placeholder);

        if(insertToString.startsWith(placeholder)){
            if(parts.length != 1) throw new RuntimeException("Invalid insertToString: " + insertToString);

            SpannableStringBuilder spannable = new SpannableStringBuilder();
            spannable.append(applyLinkStyle(linkString));
            spannable.append(parts[0]);
            return spannable;
        }

        if(insertToString.endsWith(placeholder)){
            if(parts.length != 1) throw new RuntimeException("Invalid insertToString: " + insertToString);

            SpannableStringBuilder spannable = new SpannableStringBuilder();
            spannable.append(parts[0]);
            spannable.append(applyLinkStyle(linkString));
            return spannable;
        }

        if(parts.length != 2) throw new RuntimeException("Invalid insertToString: " + insertToString);

        SpannableStringBuilder spannable = new SpannableStringBuilder();
        spannable.append(parts[0]);
        spannable.append(applyLinkStyle(linkString));
        spannable.append(parts[1]);

        return spannable;
    }


    public static Spanned applyLinkStyle(Context context, int stringRes){
        return applyLinkStyle(context.getString(stringRes));
    }

    public static Spanned applyLinkStyle(String string){
        SpannableStringBuilder spannable = new SpannableStringBuilder(string);

        int textColor = 0xff0000ff;
        spannable.setSpan(
                new ForegroundColorSpan(textColor),
                0, // start
                spannable.length(), // end
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        spannable.setSpan(
                new UnderlineSpan(),
                0, // start
                spannable.length(), // end
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        );

        return spannable;
    }

}
