package com.strategway.passim.uitools.browser;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.strategway.passim.R;
import com.strategway.passim.uitools.alert.AlertHelper;


public class BrowserHelper {


    public static void openUriInBrowser(Context context, String uri) {
        Uri webpage = Uri.parse(uri);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);

        try {
            context.startActivity(Intent.createChooser(intent, context.getString(R.string.browser_select_hint)));
        } catch (ActivityNotFoundException e){
            AlertHelper.alert(context, context.getString(R.string.browser_error, uri));
        }
    }

}
