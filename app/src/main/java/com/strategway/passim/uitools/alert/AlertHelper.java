package com.strategway.passim.uitools.alert;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.strategway.passim.R;


public class AlertHelper
{

    //****************************************
    //Ok alert

    public static void alert(Context context, int messageStringResource)
    {
        alert(context, null, context.getString(messageStringResource), null);
    }


    public static void alert(Context context, CharSequence message)
    {
        alert(context, null, message, null);
    }


    public static void alert(Context context, int messageStringResource, DialogInterface.OnClickListener clickListener)
    {
        alert(context, null, context.getString(messageStringResource), clickListener);
    }

    public static void alert(Context context, CharSequence message, DialogInterface.OnClickListener clickListener)
    {
        alert(context, null, message, clickListener);
    }


    public static void alert(Context context, int titleResource, int messageResource)
    {
        alert(context, context.getString(titleResource), context.getString(messageResource), null);
    }


    public static void alert(Context context, CharSequence title, CharSequence message)
    {
        alert(context, title, message, null);
    }

    public static void alert(Context context, CharSequence title, CharSequence message, DialogInterface.OnClickListener clickListener)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNeutralButton(R.string.ok, clickListener);

        builder.create().show();
    }


    //****************************************
    //Yes-no alert

    public static void yesNoAlert(Context context, int messageResourceString, final YesNoCallback callback)
    {
        yesNoAlert(context, context.getString(messageResourceString), callback);
    }


    public static void yesNoAlert(Context context, CharSequence message, final YesNoCallback callback){
        yesNoAlert(context, message, R.string.yes, R.string.no, callback);
    }

    public static void yesNoAlert(Context context, int messageResourceString, int yesButtonRes, int noButtonRes, final YesNoCallback callback){
        yesNoAlert(context, context.getString(messageResourceString), yesButtonRes, noButtonRes, callback);
    }

    public static void yesNoAlert(Context context, CharSequence message, int yesButtonRes, int noButtonRes, final YesNoCallback callback)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNegativeButton(
            noButtonRes, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                callback.noClicked();
            }
        }
        );

        builder.setPositiveButton(
            yesButtonRes, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                callback.yesClicked();
            }
        }
        );

        builder.create().show();
    }

    public static void optionalYesNoAlert(Context context, CharSequence message, final YesNoCallback callback)
    {
        if(message == null){
            if(callback != null) callback.yesClicked();
            return;
        }

        yesNoAlert(context, message, callback);
    }


    public interface YesNoCallback
    {

        void yesClicked();

        void noClicked();
    }


    public static class YesNoDefaultCallback implements YesNoCallback
    {

        @Override
        public void yesClicked()
        {

        }


        @Override
        public void noClicked()
        {

        }
    }

}
