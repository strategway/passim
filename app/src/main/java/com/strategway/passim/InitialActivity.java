/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.strategway.passim.core.AppOptions;
import com.strategway.passim.core.PassimBaseActivity;

/**
 * Activity of the app's first screen
 */
public class InitialActivity extends PassimBaseActivity {

    /** Duration of wait **/
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       /* New Handler to start the PreparationActivity
         * and close this Splash-Screen after some seconds.*/
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the PreparationActivity. */
                Intent intent = new Intent(InitialActivity.this, PreparationActivity.class);
                InitialActivity.this.startActivity(intent);
                InitialActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_initial;
    }
}
