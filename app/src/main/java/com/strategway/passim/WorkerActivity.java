/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.strategway.passim.core.GPSTracker;
import com.strategway.passim.core.ILocationObserver;
import com.strategway.passim.core.PassimBaseActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class WorkerActivity extends PassimBaseActivity implements ILocationObserver, ActivityCompat.OnRequestPermissionsResultCallback {

    // passengers count variables
    private int currentPassengersInCount = 0;
    private int currentPassengersOutCount = 0;

    // ui controls
    private TextView currentPassengersInTextView;
    private TextView currentPassengersOutTextView;
    private Button routeTaxiStoppedButton;
    private Button passengersInMinusButton;
    private Button passengersInPlusButton;
    private Button passengersOutMinusButton;
    private Button passengersOutPlusButton;
    private Button cancelButton;
    private Button saveButton;
    private Button finishRouteButton;
    private ImageView satelliteImageView;
    private ImageView networkImageView;

    // flag that freezes current coordinates
    private boolean isTaxiStopped = false;

    // current coordinates
    private double currentLatitude = -1;
    private double currentLongitude = -1;

    // providers flag
    private boolean isGPSOn;
    private boolean isNetworkOn;

    // Progress dialogs
    private ProgressDialog authDialogProgress;
    private ProgressDialog uploadDialogProgress;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize screen controls
        currentPassengersInTextView = (TextView) findViewById(R.id.passengersInTextView);
        currentPassengersOutTextView = (TextView) findViewById(R.id.passengersOutTextView);
        routeTaxiStoppedButton = (Button) findViewById(R.id.routeTaxiStoppedButton);
        passengersInMinusButton = (Button) findViewById(R.id.passengersInMinusButton);
        passengersInPlusButton = (Button) findViewById(R.id.passengersInPlusButton);
        passengersOutMinusButton = (Button) findViewById(R.id.passengersOutMinusButton);
        passengersOutPlusButton = (Button) findViewById(R.id.passengersOutPlusButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);
        saveButton = (Button) findViewById(R.id.saveButton);
        finishRouteButton = (Button) findViewById(R.id.finishRouteButton);
        satelliteImageView = (ImageView) findViewById(R.id.satelliteImageView);
        networkImageView = (ImageView) findViewById(R.id.networkImageView);

        // default properties for location providers statuses
        isGPSOn = false;
        isNetworkOn = false;
        satelliteImageView.setBackgroundColor(Color.RED);
        networkImageView.setBackgroundColor(Color.RED);

        // set properties of the controls
        cancelButton.setEnabled(false);
        saveButton.setEnabled(false);
        routeTaxiStoppedButton.setEnabled(false);
        currentPassengersInTextView.setText(String.valueOf(currentPassengersInCount));
        currentPassengersOutTextView.setText(String.valueOf(currentPassengersOutCount));

        // setting onClickListener for the buttons of the screen

        // freezes coordinates
        routeTaxiStoppedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // set flag to freeze coordinates
                isTaxiStopped = true;

                // set availability of the buttons
                saveButton.setEnabled(true);
                routeTaxiStoppedButton.setEnabled(false);
                cancelButton.setEnabled(true);
            }
        });

        // cancels previous input
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // set current params to zero
                currentPassengersInCount = 0;
                currentPassengersOutCount = 0;
                currentPassengersInTextView.setText(String.valueOf(currentPassengersInCount));
                currentPassengersOutTextView.setText(String.valueOf(currentPassengersOutCount));

                // set availability of the buttons
                cancelButton.setEnabled(false);

                if(isTaxiStopped) {
                    isTaxiStopped = false;
                    routeTaxiStoppedButton.setEnabled(true);
                    saveButton.setEnabled(false);
                }
            }
        });

        // saves data to file
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // save changes to file
                if(currentLatitude != -1 && currentLongitude != -1) {
                    appOptions.saveChangesToFile(currentPassengersInCount, currentPassengersOutCount, currentLatitude, currentLongitude);
                }

                // set current params to zero
                currentPassengersInCount = 0;
                currentPassengersOutCount = 0;
                currentPassengersInTextView.setText(String.valueOf(currentPassengersInCount));
                currentPassengersOutTextView.setText(String.valueOf(currentPassengersOutCount));

                // set availability of the buttons
                cancelButton.setEnabled(false);
                isTaxiStopped = false;
                routeTaxiStoppedButton.setEnabled(true);
                saveButton.setEnabled(false);

                if(currentLatitude == -1 && currentLongitude == -1) {
                    ((TextView) findViewById(R.id.currentCoordinatesTextView)).setText("");
                    routeTaxiStoppedButton.setEnabled(false);
                }
            }
        });

        // uploads file on server
        finishRouteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AlertDialog.Builder(WorkerActivity.this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle(getString(R.string.route_ending))
                        .setMessage(getString(R.string.route_ending_confirmation))
                                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        uploadFile(appOptions.getResultFileName());
                                    }

                                })
                                .setNegativeButton(getString(R.string.no), null)
                                .show();
            }
        });

        appOptions.getGPSTracker().addObserver(this);
    }

    public void uploadFile(final String filename) {
           appOptions.client.uploadFile(appOptions.getWorkerLogin(), appOptions.getWorkerPassword(),
                new File(getBaseContext().getFilesDir() + File.separator + filename),
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                        // copy file to another folder in internal memory
                        appOptions.copyFileToUploadedFolder(filename);

                        goToPrepsScreen(filename);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                        String message;

                        if(responseBody == null) {
                            message = appOptions.client.getConnectionErrorMessage(WorkerActivity.this);
                        }
                        else {
                            message = appOptions.client.getFileSendingErrorMessage(WorkerActivity.this);
                        }

                        AlertDialog.Builder alert = new AlertDialog.Builder(WorkerActivity.this)
                                .setTitle(R.string.application_error)
                                .setMessage(message);
                        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dialog.cancel();
                                goToPrepsScreen();
                            }
                        });
                        alert.show();
                    }

                    @Override
                    public void onStart() {

                        uploadDialogProgress = ProgressDialog.show(WorkerActivity.this, getResources().getString(R.string.file_uploading_progress_message_title),
                                getResources().getString(R.string.file_uploading_progress_message), true);
                    }

                    @Override
                    public void onFinish() {
                        uploadDialogProgress.dismiss();
                    }
                });
    }

    @Override
    public void onLocationChanged(Location location) {

        // change current longitude and latitude params if they're not frozen
        if(!isTaxiStopped) {
            currentLatitude = location.getLatitude();
            currentLongitude = location.getLongitude();
            ((TextView) findViewById(R.id.currentCoordinatesTextView)).setText(String.valueOf(currentLatitude) + " " +
                    String.valueOf(currentLongitude) + "\n" + getString(R.string.geolocation_accuracy) + " " + location.getAccuracy());
            if(!routeTaxiStoppedButton.isEnabled())
                routeTaxiStoppedButton.setEnabled(true);
        }

        String provider = location.getProvider();
        if(provider.equals(LocationManager.GPS_PROVIDER)) {
            if(!isGPSOn) {
                isGPSOn = true;
                satelliteImageView.setBackgroundColor(Color.GREEN);
            }
        }
        else if(provider.equals(LocationManager.NETWORK_PROVIDER)) {
            if(!isNetworkOn) {
                isNetworkOn = true;
                networkImageView.setBackgroundColor(Color.GREEN);
            }
        }
    }

    @Override
    public void onGPSFixed() {
        Toast.makeText(this, getResources().getString(R.string.geolocation_gps_first_fix), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGPSStarted() {
        //Toast.makeText(this, getResources().getString(R.string.geolocation_start_gps), Toast.LENGTH_SHORT).show();
        satelliteImageView.setBackgroundColor(Color.GREEN);

        isGPSOn = true;

        if(!isTaxiStopped) {
            routeTaxiStoppedButton.setEnabled(true);
        }
    }

    @Override
    public void onGPSStopped() {
        isGPSOn = false;

        checkLocationAvailability();
        //Toast.makeText(this, getResources().getString(R.string.geolocation_disabled), Toast.LENGTH_SHORT).show();
        satelliteImageView.setBackgroundColor(Color.RED);
    }

    @Override
    public void onNetworkStopped() {
        isNetworkOn = false;
        checkLocationAvailability();
        networkImageView.setBackgroundColor(Color.RED);
    }

    @Override
    public void onNetworkStarted() {
        isNetworkOn = true;
        networkImageView.setBackgroundColor(Color.GREEN);
    }

    private void checkLocationAvailability() {
        if(!isNetworkOn && !isGPSOn) {
            Toast.makeText(this, getString(R.string.geolocation_disabled), Toast.LENGTH_SHORT).show();
            currentLatitude = -1;
            currentLongitude = -1;
            if(!isTaxiStopped) {
                ((TextView) findViewById(R.id.currentCoordinatesTextView)).setText("");
                routeTaxiStoppedButton.setEnabled(false);
                saveButton.setEnabled(false);
            }
            else {
                routeTaxiStoppedButton.setEnabled(false);
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        appOptions.getGPSTracker().removeObserver(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((TextView) findViewById(R.id.currentCoordinatesTextView)).setText("");
    }

    /**
     * Change passengers count params
     * @param view
     */
    public void onChangePassengersCountClick(View view) {
        switch (view.getId()) {
            case R.id.passengersInMinusButton:
                if(currentPassengersInCount > 0)
                    currentPassengersInCount--;
                break;
            case R.id.passengersInPlusButton:
                currentPassengersInCount++;
                break;
            case R.id.passengersOutMinusButton:
                if(currentPassengersOutCount > 0)
                    currentPassengersOutCount--;
                break;
            case R.id.passengersOutPlusButton:
                currentPassengersOutCount++;
                break;
            default:
                break;
        }

        currentPassengersInTextView.setText(String.valueOf(currentPassengersInCount));
        currentPassengersOutTextView.setText(String.valueOf(currentPassengersOutCount));

        if(currentPassengersInCount == 0 && currentPassengersOutCount == 0 && !isTaxiStopped) {
            cancelButton.setEnabled(false);
        } else {
            cancelButton.setEnabled(true);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_worker;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case GPSTracker.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    appOptions.getGPSTracker().requestLocationUpdates();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(appOptions.getCurrentActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(WorkerActivity.this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.quit))
                .setMessage(getString(R.string.quit_confirmation))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        WorkerActivity.this.finish();
                    }

                })
                .setNegativeButton(getString(R.string.no), null)
                .show();
    }

    private void goToPrepsScreen() {
        Intent intent = new Intent(WorkerActivity.this, PreparationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void goToPrepsScreen(String fileName) {
        Intent intent = new Intent(WorkerActivity.this, PreparationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("send", fileName);
        startActivity(intent);
    }
}
