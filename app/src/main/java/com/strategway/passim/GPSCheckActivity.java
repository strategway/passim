/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim;

import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.strategway.passim.core.GPSTracker;
import com.strategway.passim.core.ILocationObserver;
import com.strategway.passim.core.PassimBaseActivity;

public class GPSCheckActivity extends PassimBaseActivity implements ILocationObserver, ActivityCompat.OnRequestPermissionsResultCallback {

    // properties
    private ImageView satelliteImageView;
    private ImageView networkImageView;

    // providers flag
    private boolean isGPSOn;
    private boolean isNetworkOn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        satelliteImageView = (ImageView) findViewById(R.id.satelliteImageView);
        networkImageView = (ImageView) findViewById(R.id.networkImageView);

        // set coordinates
        double latitude = appOptions.getGPSTracker().getLatitude();
        double longitude = appOptions.getGPSTracker().getLongitude();

        if(latitude != -1 && longitude != -1) {
            ((TextView) findViewById(R.id.coordinatesTextView)).setText(String.valueOf(appOptions.getGPSTracker().getLatitude()) + " " +
                    String.valueOf(appOptions.getGPSTracker().getLongitude()));
            ((Button)findViewById(R.id.buttonForward)).setEnabled(true);
        }
        else {
            ((TextView) findViewById(R.id.coordinatesTextView)).setText(getResources().getString(R.string.geolocation_disabled));
        }

        appOptions.getGPSTracker().addObserver(this);

        //Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        ((TextView) findViewById(R.id.coordinatesTextView)).setText(String.valueOf(location.getLatitude()) + " " +
                String.valueOf(location.getLongitude()) + "\n" + getString(R.string.geolocation_accuracy) + " " + location.getAccuracy());
        ((Button)findViewById(R.id.buttonForward)).setEnabled(true);

        String provider = location.getProvider();
        if(provider.equals(LocationManager.GPS_PROVIDER)) {
            if(!isGPSOn) {
                isGPSOn = true;
                satelliteImageView.setBackgroundColor(Color.GREEN);
            }
        }
        else if(provider.equals(LocationManager.NETWORK_PROVIDER)) {
            if(!isNetworkOn) {
                isNetworkOn = true;
                networkImageView.setBackgroundColor(Color.GREEN);
            }
        }
    }

    @Override
    public void onGPSFixed() {
        Toast.makeText(this, getResources().getString(R.string.geolocation_gps_first_fix), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGPSStarted() {
        //Toast.makeText(this, getResources().getString(R.string.geolocation_start_gps), Toast.LENGTH_SHORT).show();

        satelliteImageView.setBackgroundColor(Color.GREEN);
        isGPSOn = true;
    }

    @Override
    public void onGPSStopped() {
        Toast.makeText(this, getResources().getString(R.string.geolocation_disabled), Toast.LENGTH_SHORT).show();
        ((TextView) findViewById(R.id.coordinatesTextView)).setText("");
        ((Button)findViewById(R.id.buttonForward)).setEnabled(false);

        isGPSOn = false;
        checkLocationAvailability();
    }

    @Override
    public void onNetworkStopped() {
        isNetworkOn = false;
        checkLocationAvailability();
        networkImageView.setBackgroundColor(Color.RED);
    }

    @Override
    public void onNetworkStarted() {
        isNetworkOn = true;
        networkImageView.setBackgroundColor(Color.GREEN);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_gpscheck;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((Button)findViewById(R.id.buttonForward)).setEnabled(false);
        ((TextView) findViewById(R.id.coordinatesTextView)).setText("");
        // default properties for location providers statuses
        isGPSOn = false;
        isNetworkOn = false;
        satelliteImageView.setBackgroundColor(Color.RED);
        networkImageView.setBackgroundColor(Color.RED);

        //Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        appOptions.getGPSTracker().removeObserver(this);
        //Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
    }

    private void checkLocationAvailability() {
        if(!isNetworkOn && !isGPSOn) {
            ((TextView) findViewById(R.id.coordinatesTextView)).setText("");
            Toast.makeText(this, getString(R.string.geolocation_disabled), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case GPSTracker.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    appOptions.getGPSTracker().requestLocationUpdates();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(appOptions.getCurrentActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }
}
