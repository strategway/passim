/*
 * Copyright (C) 2015-2024 Strategway, http://strategway.com
 *
 * This file is part of "Passim", the Android mobile application to gather public
 * transport passenger data which can be later viewed with "Jupiter" web application.
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.strategway.passim;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.strategway.passim.core.PassimBaseActivity;
import com.strategway.passim.core.Route;


/**
 * Activity of the screen where user inputs all the data required to work
 */
public class PreparationActivity extends PassimBaseActivity {

    // region Properties

    // shared preferences variables
    public static final String PREFS_NAME = "PassimPrefsFile";
    public SharedPreferences settings;

    // text fields
    private EditText routeEditText;
    private EditText startStationEditText;
    private EditText terminalStationEditText;

    private ProgressDialog dialogProgress;

    // endregion

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // initialize text fields
        routeEditText = (EditText) findViewById(R.id.routeEditText);
        startStationEditText = (EditText) findViewById(R.id.startStationEditText);
        terminalStationEditText = (EditText) findViewById(R.id.terminalStationEditText);

        // set listeners
        routeEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    return;
                }

                settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(getString(R.string.route_number_key), s.toString());
                editor.commit();
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        startStationEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    return;
                }

                settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(getString(R.string.start_station_key), s.toString());
                editor.commit();
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        terminalStationEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()) {
                    return;
                }

                settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(getString(R.string.terminal_station_key), s.toString());
                editor.commit();

                appOptions.setTerminalStation(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        // send a letter to foreman's email if needed
        String fileName = getIntent().getStringExtra("send");
        if (fileName != null) {
            appOptions.sendConfirmationToBossEmail(fileName, true);
            getIntent().removeExtra("send");
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_preparation;
    }

    @Override
    public void onResume() {
        super.onResume();

        // initialize settings
        settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

        if (!settings.contains(getResources().getString(R.string.worker_name_key)) ||
                !settings.contains(getResources().getString(R.string.worker_email_key)) ||
                !settings.contains(getResources().getString(R.string.boss_email_key)) ||
                !settings.contains(getResources().getString(R.string.password)) ||
                !settings.contains(getResources().getString(R.string.login)) ||
                !settings.contains(getResources().getString(R.string.server_name))) {

            Intent intent = new Intent(this, UserParamsActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            String workerName  = settings.getString(getResources().getString(R.string.worker_name_key), "");
            String workerEmail = settings.getString(getResources().getString(R.string.worker_email_key), "");
            String bossEmail = settings.getString(getResources().getString(R.string.boss_email_key), "");
            String workerLogin = settings.getString(getResources().getString(R.string.login), "");
            String workerPassword = settings.getString(getResources().getString(R.string.password), "");

            // save params and settings
            appOptions.setWorkerName(workerName);
            appOptions.setWorkerEmail(workerEmail);
            appOptions.setBossEmail(bossEmail);
            appOptions.setWorkerLogin(workerLogin);
            appOptions.setWorkerPassword(workerPassword);
        }

        // try to get route info saved earlier
        if (settings.contains(getResources().getString(R.string.route_number_key))) {
            routeEditText.setText(settings.getString(getResources().getString(R.string.route_number_key), ""));
        }

        if (settings.contains(getResources().getString(R.string.start_station_key))) {
            startStationEditText.setText(settings.getString(getResources().getString(R.string.start_station_key), ""));
        }

        if (settings.contains(getResources().getString(R.string.terminal_station_key))) {
            terminalStationEditText.setText(settings.getString(getResources().getString(R.string.terminal_station_key), ""));
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * Saves route number and terminal station in app options.
     * @return <code>true</code>, if user's input is correct (not empty), <code>false</code> otherwise.
     */
    public boolean saveUserInputToOptions() {

        if (routeEditText.getText().toString().isEmpty() ||
                startStationEditText.getText().toString().isEmpty() ||
                terminalStationEditText.getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.strings_should_not_be_empty_message, Toast.LENGTH_SHORT).show();
            return false;
        }

        String route = routeEditText.getText().toString();
        String startStation = startStationEditText.getText().toString();
        String terminalStation = terminalStationEditText.getText().toString();

        appOptions.setCurrentRoute(new Route(route, startStation, terminalStation));
        appOptions.setTerminalStation(terminalStation);

        return true;
    }

    public void onSwapStationsClick(View view) {
        String startStation = startStationEditText.getText().toString();
        String terminalStation = terminalStationEditText.getText().toString();

        startStationEditText.setText(terminalStation);
        terminalStationEditText.setText(startStation);
    }
}
